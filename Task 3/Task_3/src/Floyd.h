#pragma once


#include "Graph.h"


class Floyd {
 private:

  Graph G;
  
  size_t size_of_graph;
  
  std::vector <std::vector <double> > smallest_ways;

 public:
  
  Floyd(Graph &);

  void algorithm_Floyd();

  std::vector <std::vector <double> > get_smallest_way();

  void print_smallest_ways_in_file();
};