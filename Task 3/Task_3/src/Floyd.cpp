#include <vector>
#include "Floyd.h"


Floyd::Floyd(Graph & G) {
  size_of_graph = G.get_count_of_vertices();
  smallest_ways = G.get_adjancy_matrix();
}

void Floyd::algorithm_Floyd() {
  for (size_t k = 0; k < size_of_graph; k++)
    for (size_t i = 0; i < size_of_graph; i++)
      for (size_t j = 0; j < size_of_graph; j++)
        if (smallest_ways[i][j] > smallest_ways[i][k] + smallest_ways[k][j]) {
          smallest_ways[i][j] = smallest_ways[i][k] + smallest_ways[k][j];
        }
}

void Floyd::print_smallest_ways_in_file() {
  std::ofstream file("output.txt");
  if (!file) {
    std::cerr << "Can`t open file!" << std::endl;
    exit(1);
  }
  for (int i = 0; i < smallest_ways.size(); i++) {
    for (int j = 0; j < smallest_ways[i].size(); j++)
      file << smallest_ways[i][j] << ' ';
      file << std::endl;
  }
  file.close();
}

std::vector<std::vector<double>> Floyd::get_smallest_way() {
  return smallest_ways;
}
