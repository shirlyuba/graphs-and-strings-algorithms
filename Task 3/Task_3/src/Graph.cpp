#include <vector>
#include "Graph.h"

Graph::Graph() {};

Graph::Graph(size_t new_count_of_vertices) {
  count_of_vertices = new_count_of_vertices;
	adjancy_matrix.resize(count_of_vertices);
}

std::vector <std::vector <double> > Graph::read_parameters_from_stdin(size_t N ) {
  count_of_vertices = N;
  std::vector <std::vector <double> > result;
  std::string line_of_parametres;
  std::getline(std::cin, line_of_parametres);
  for (int i = 0; i < count_of_vertices; i++) {
    int parametr;
    std::getline(std::cin, line_of_parametres);
    std::istringstream iss(line_of_parametres);
    while (iss >> parametr) {
      result[i].push_back(parametr);
    }
  }
  return result;
}

void Graph::read_adjancy_list_from_stdin() {
  adjancy_list = read_parameters_from_stdin(count_of_vertices);
  for (int i = 0; i < count_of_vertices; i++)
    for (int j = 0; j < adjancy_list[i].size(); j++) {
      create_edge(i, adjancy_list[i][j]);
      adjancy_matrix[i][adjancy_list[i][j]] = 1;
    }
}

void Graph::read_adjancy_matrix_from_stdin() {
  adjancy_matrix = read_parameters_from_stdin(count_of_vertices);
  for (int i = 0; i < count_of_vertices; i++)
    for (int j = 0; j < adjancy_matrix.size(); j++) {
      create_edge(i, j, adjancy_matrix[i][j]);
      adjancy_list[i].push_back(j);
    }
}

void Graph::create_edge(int start, int finish, double value) {
  Edge new_edge;
  new_edge.start = start;
  new_edge.finish = finish;
  new_edge.value = value;
  edges.push_back(new_edge);
}

double Graph::get_value(int start, int finish) {
  for (auto item : edges) {
    if (item.start == start && item.finish == finish)
      return item.value;
  }
}

bool Graph::existance_of_edge(int start, int finish) {
  for (int i = 0; i < edges.size(); i++) {
    if (edges[i].start == start && edges[i].finish == finish)
      return true;
  }
  return false;
}

void Graph::set_count_of_vertices(size_t N) {
  count_of_vertices = N;
}

size_t Graph::get_count_of_vertices() {
  return count_of_vertices;
}

std::vector <std::vector <double> > Graph::get_adjancy_matrix() {
  return adjancy_matrix;
}

std::vector <std::vector <double> > Graph::get_adjancy_list() {
  return adjancy_list;
}

void Graph::read_adjancy_matrix_from_file() {
  std::ifstream file("input.txt");
  std::string line_of_parametres;
  file >> count_of_vertices;
  set_count_of_vertices(count_of_vertices);
  adjancy_matrix.resize(count_of_vertices);
  std::getline(file, line_of_parametres);
  while (!file.eof()) {
    for (size_t i = 0; i < count_of_vertices; i++) {
      int parametr;
      std::getline(file, line_of_parametres);
      std::istringstream iss(line_of_parametres);
      while (iss >> parametr) {
        adjancy_matrix[i].push_back(parametr);
      }
    }
  }
  file.close();
}

void Graph::print_adjancy_matrix_in_stdout() {
  for (int i = 0; i < adjancy_matrix.size(); i++) {
    for (int j = 0; j < adjancy_matrix.size(); j++)
      std::cout << adjancy_matrix[i][j];
      std::cout << std::endl;
  }
}

void Graph::print_adjancy_list_in_stdout() {
  for (int i = 0; i < adjancy_list.size(); i++) {
    for (int j = 0; j < adjancy_list[i].size(); j++)
      std::cout << adjancy_list[i][j];
  std::cout << std::endl;
  }
}

void Graph::create_adjancy_matrix() {
	for (int i = 0; i < count_of_vertices; i++) {
		for (int j = 0; j < count_of_vertices; j++) {
			adjancy_matrix[i].push_back(get_value(i, j));
		}
	}
}
