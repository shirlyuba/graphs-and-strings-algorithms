#include "Graph.h"
#include "Floyd.h"


int main() {
  Graph graph;
  graph.read_adjancy_matrix_from_file();
  Floyd floyd_to_graph(graph);
  floyd_to_graph.algorithm_Floyd();
  floyd_to_graph.print_smallest_ways_in_file();
  return 0;
}