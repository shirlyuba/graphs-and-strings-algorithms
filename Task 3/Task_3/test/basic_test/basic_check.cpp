#include <Graph.h>
#include <Floyd.h>

#include "gtest/gtest.h"


TEST(basic_test, test_1) {
	Graph G(4);
	G.create_edge(0, 0);
	G.create_edge(0, 1, 5);
	G.create_edge(0, 2, 9);
	G.create_edge(0, 3, 100);
	G.create_edge(1, 0, 100);
	G.create_edge(1, 1);
	G.create_edge(1, 2, 2);
	G.create_edge(1, 3, 8);
	G.create_edge(2, 0, 100);
	G.create_edge(2, 1, 100);
	G.create_edge(2, 2);
	G.create_edge(2, 3, 7);
	G.create_edge(3, 0, 4);
	G.create_edge(3, 1, 100);
	G.create_edge(3, 2, 100);
	G.create_edge(3, 3);
	G.create_adjancy_matrix();
	Floyd F(G);
	F.algorithm_Floyd();
	EXPECT_EQ(5, F.get_smallest_way()[0][1]);
	EXPECT_EQ(7, F.get_smallest_way()[0][2]);
	EXPECT_EQ(13, F.get_smallest_way()[0][3]);
	EXPECT_EQ(12, F.get_smallest_way()[1][0]);
	EXPECT_EQ(2, F.get_smallest_way()[1][2]);
	EXPECT_EQ(8, F.get_smallest_way()[1][3]);
	EXPECT_EQ(11, F.get_smallest_way()[2][0]);
	EXPECT_EQ(16, F.get_smallest_way()[2][1]);
	EXPECT_EQ(7, F.get_smallest_way()[2][3]);
	EXPECT_EQ(4, F.get_smallest_way()[3][0]);
	EXPECT_EQ(9, F.get_smallest_way()[3][1]);
	EXPECT_EQ(11, F.get_smallest_way()[3][2]);
}

TEST(basic_test, test_2) {
	Graph G(5);
	G.create_edge(0, 0);
	G.create_edge(0, 1, 11);
	G.create_edge(0, 2, 12);
	G.create_edge(0, 3, 3);
	G.create_edge(0, 4, 43);
	G.create_edge(1, 0, 14);
	G.create_edge(1, 1);
	G.create_edge(1, 2, 67);
	G.create_edge(1, 3, 1);
	G.create_edge(1, 4, 18);
	G.create_edge(2, 0, 10);
	G.create_edge(2, 1, 120);
	G.create_edge(2, 2);
	G.create_edge(2, 3, 7);
	G.create_edge(2, 4, 78);
	G.create_edge(3, 0, 42);
	G.create_edge(3, 1, 12);
	G.create_edge(3, 2, 1000);
	G.create_edge(3, 3);
	G.create_edge(3, 4, 34);
	G.create_edge(4, 0, 12);
	G.create_edge(4, 1, 99);
	G.create_edge(4, 2, 374);
	G.create_edge(4, 3, 32);
	G.create_edge(4, 4);
	G.create_adjancy_matrix();
	Floyd F(G);
	F.algorithm_Floyd();
	EXPECT_EQ(11, F.get_smallest_way()[0][1]);
	EXPECT_EQ(12, F.get_smallest_way()[0][2]);
	EXPECT_EQ(3, F.get_smallest_way()[0][3]);
	EXPECT_EQ(29, F.get_smallest_way()[0][4]);
	EXPECT_EQ(14, F.get_smallest_way()[1][0]);
	EXPECT_EQ(26, F.get_smallest_way()[1][2]);
	EXPECT_EQ(1, F.get_smallest_way()[1][3]);
	EXPECT_EQ(18, F.get_smallest_way()[1][4]);
	EXPECT_EQ(10, F.get_smallest_way()[2][0]);
	EXPECT_EQ(19, F.get_smallest_way()[2][1]);
	EXPECT_EQ(7, F.get_smallest_way()[2][3]);
	EXPECT_EQ(37, F.get_smallest_way()[2][4]);
	EXPECT_EQ(26, F.get_smallest_way()[3][0]);
	EXPECT_EQ(12, F.get_smallest_way()[3][1]);
	EXPECT_EQ(38, F.get_smallest_way()[3][2]);
	EXPECT_EQ(30, F.get_smallest_way()[3][4]);
	EXPECT_EQ(12, F.get_smallest_way()[4][0]);
	EXPECT_EQ(23, F.get_smallest_way()[4][1]);
	EXPECT_EQ(24, F.get_smallest_way()[4][2]);
	EXPECT_EQ(15, F.get_smallest_way()[4][3]);
}

TEST(basic_test, test_3) {
	Graph G(3);
	G.create_edge(0, 0);
	G.create_edge(0, 1, 10);
	G.create_edge(0, 2, 5);
	G.create_edge(1, 0, 3);
	G.create_edge(1, 1);
	G.create_edge(1, 2, 9);
	G.create_edge(2, 0, 6);
	G.create_edge(2, 1, 4);
	G.create_edge(2, 2);
	G.create_adjancy_matrix();
	Floyd F(G);
	F.algorithm_Floyd();
	EXPECT_EQ(9, F.get_smallest_way()[0][1]);
	EXPECT_EQ(5, F.get_smallest_way()[0][2]);
	EXPECT_EQ(3, F.get_smallest_way()[1][0]);
	EXPECT_EQ(8, F.get_smallest_way()[1][2]);
	EXPECT_EQ(6, F.get_smallest_way()[2][0]);
	EXPECT_EQ(4, F.get_smallest_way()[2][1]);
}
