#include "gtest/gtest.h"
#include <Treap.h>

TEST(basic_test, test_1) {
	std::shared_ptr <Treap> treap = std::make_shared<Treap>(Treap(5, 11));
	std::shared_ptr <Treap> bstree = std::make_shared<Treap>(Treap(5, 0));
	treap->insert(18, 8, treap);
	bstree->insert(18, 0, bstree);
	treap->insert(25, 7, treap);
	bstree->insert(25, 0, bstree);
	treap->insert(50, 12, treap);
	bstree->insert(50, 0, bstree);
	treap->insert(30, 30, treap);
	bstree->insert(30, 0, bstree);
	treap->insert(15, 15, treap);
	bstree->insert(15, 0, bstree);
	treap->insert(20, 10, treap);
	bstree->insert(20, 0, bstree);
	treap->insert(22, 5, treap);
	bstree->insert(22, 0, bstree);
	treap->insert(40, 20, treap);
	bstree->insert(40, 0, bstree);
	treap->insert(45, 9, treap);
	bstree->insert(45, 0, bstree);
	EXPECT_EQ(bstree->get_depth(bstree) - treap->get_depth(treap), 2);
}

TEST(basic_test, test_2) {
	std::shared_ptr <Treap> treap = std::make_shared<Treap>(Treap(38, 19));
	std::shared_ptr <Treap> bstree = std::make_shared<Treap>(Treap(38, 0));
	treap->insert(37, 5, treap);
	bstree->insert(37, 0, bstree);
	treap->insert(47, 15, treap);
	bstree->insert(47, 0, bstree);
	treap->insert(35, 0, treap);
	bstree->insert(35, 0, bstree);
	treap->insert(12, 3, treap);
	bstree->insert(12, 0, bstree);
	treap->insert(0, 42, treap);
	bstree->insert(0, 0, bstree);
	treap->insert(31, 37, treap);
	bstree->insert(31, 0, bstree);
	treap->insert(21, 45, treap);
	bstree->insert(21, 0, bstree);
	treap->insert(30, 26, treap);
	bstree->insert(30, 0, bstree);
	treap->insert(41, 6, treap);
	bstree->insert(41, 0, bstree);
	EXPECT_EQ(bstree->get_depth(bstree) - treap->get_depth(treap), 2);
}

TEST(basic_test, test_3) {
	std::shared_ptr <Treap> treap = std::make_shared<Treap>(Treap(38, 19));
	std::shared_ptr <Treap> bstree = std::make_shared<Treap>(Treap(38, 0));
	treap->insert(37, 5, treap);
	bstree->insert(37, 0, bstree);
	treap->insert(47, 15, treap);
	bstree->insert(47, 0, bstree);
	treap->insert(35, 0, treap);
	bstree->insert(35, 0, bstree);
	treap->insert(12, 3, treap);
	bstree->insert(12, 0, bstree);
	treap->insert(0, 42, treap);
	bstree->insert(0, 0, bstree);
	treap->insert(31, 37, treap);
	bstree->insert(31, 0, bstree);
	treap->insert(21, 45, treap);
	bstree->insert(21, 0, bstree);
	treap->insert(30, 26, treap);
	bstree->insert(30, 0, bstree);
	treap->insert(41, 6, treap);
	bstree->insert(41, 0, bstree);
	EXPECT_EQ(treap->get_width(treap) - bstree->get_width(bstree), 1);
}

TEST(basic_test, test_4) {
	std::shared_ptr <Treap> treap = std::make_shared<Treap>(Treap(5, 11));
	std::shared_ptr <Treap> bstree = std::make_shared<Treap>(Treap(5, 0));
	treap->insert(18, 8, treap);
	bstree->insert(18, 0, bstree);
	treap->insert(25, 7, treap);
	bstree->insert(25, 0, bstree);
	treap->insert(50, 12, treap);
	bstree->insert(50, 0, bstree);
	treap->insert(30, 30, treap);
	bstree->insert(30, 0, bstree);
	treap->insert(15, 15, treap);
	bstree->insert(15, 0, bstree);
	treap->insert(20, 10, treap);
	bstree->insert(20, 0, bstree);
	treap->insert(22, 5, treap);
	bstree->insert(22, 0, bstree);
	treap->insert(40, 20, treap);
	bstree->insert(40, 0, bstree);
	treap->insert(45, 9, treap);
	bstree->insert(45, 0, bstree);
	EXPECT_EQ(treap->get_width(treap) - bstree->get_width(bstree), 1);
}