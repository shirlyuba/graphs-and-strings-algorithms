#include <iostream>


#include "Treap.h"


int main() {
	long count_of_nodes;
	std::cin >> count_of_nodes;
	int key;
	int priority;
	std::cin >> key >> priority;
	std::shared_ptr <Treap> treap = std::make_shared<Treap>(Treap(key, priority));
	std::shared_ptr <Treap> bstree = std::make_shared<Treap>(Treap(key, 0));
	for (long i = 1; i < count_of_nodes; ++i) {
		std::cin >> key >> priority;
		treap->insert(key, priority, treap);
		bstree->insert(key, 0, bstree);
	}
	std::cout << bstree->get_depth(bstree) - treap->get_depth(treap);
}