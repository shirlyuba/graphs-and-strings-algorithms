#include <vector>
#include "Treap.h"


void Treap::split(int key, std::shared_ptr <Treap> current_node, std::shared_ptr <Treap> & left,
                  std::shared_ptr <Treap> & right) {
	if (current_node == nullptr) {
		left = nullptr;
		right = nullptr;
	} else {
		if (current_node->key <= key) {
			split(key, current_node->right, current_node->right, right);
			left = current_node;
		} else {
			split(key, current_node->left, left, current_node->left);
			right = current_node;
		}
	}
}

std::shared_ptr <Treap> Treap::merge(std::shared_ptr <Treap> left, std::shared_ptr <Treap> right) {
	if (right == nullptr) {
		return left;
	}
	if (left == nullptr) {
		return right;
	}
	if (left->priority > right->priority) {
		left->right = merge(left->right, right);
		return left;
	}
	right->left = merge(left, right->left);
	return right;
}

void Treap::insert(int key, int priority, std::shared_ptr <Treap> & tree) {
	if (tree == nullptr) {
		tree = std::make_shared <Treap> (Treap(key, priority));
	} else {
		if (tree->priority >= priority) {
			if (key >= tree->key) {
				insert(key, priority, tree->right);
			} else {
				insert(key, priority, tree->left);
			}
		} else {
			std::shared_ptr <Treap> left = nullptr;
			std::shared_ptr <Treap> right = nullptr;
			split(key, tree, left, right);
			tree = std::make_shared <Treap> (Treap(key, priority, left, right));
		}
	}
}

Treap::Treap(int key, int priority, std::shared_ptr <Treap> left, std::shared_ptr <Treap> right) {
	this->key = key;
	this->priority = priority;
	this->left = left;
	this->right = right;
}

int Treap::get_depth(std::shared_ptr<Treap> tree) {
	int left_depth = 0;
	int right_depth = 0;
	if (tree == nullptr) {
		return 0;
	}
	if (tree->left != nullptr) {
		left_depth = get_depth(tree->left);
	}
	if (tree->right != nullptr) {
		right_depth = get_depth(tree->right);
	}
	return(std::max(left_depth, right_depth) + 1);
}

void Treap::erase(int key, std::shared_ptr <Treap> & tree) {
	if (tree == nullptr) {
		return;
	}
	if (key < tree->key) {
		erase(key, tree->left);
	}
	if (key > tree->key) {
		erase(key, tree->right);
	}
	if (key == tree->key) {
		tree = merge(tree->left, tree->right);
	}
}

void Treap::get_width_of_level(std::shared_ptr <Treap> tree, int level, std::vector<int> & current_level) {
	if (tree != nullptr) {
		if (current_level.size() < level + 1) {
			current_level.push_back(1);
		} else {
			++current_level[level];
		}
		get_width_of_level(tree->left, level + 1, current_level);
		get_width_of_level(tree->right, level + 1, current_level);
	}
}

int Treap::get_width(std::shared_ptr <Treap> tree) {
	int width;
	std::vector <int> number_of_vertices_on_level;
	get_width_of_level(tree, 0, number_of_vertices_on_level);
	int max = number_of_vertices_on_level[0];
	for (auto item : number_of_vertices_on_level) {
		if (max < item) {
			max = item;
		}
	}
	return max;
}

