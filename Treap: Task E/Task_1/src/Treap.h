#pragma once


#include <memory>


class Treap {

 private:

 int key;

 int priority;

 std::shared_ptr <Treap> left;

 std::shared_ptr <Treap> right;

 public:

 void insert(int key, int priority, std::shared_ptr <Treap> & tree);

 void split(int key, std::shared_ptr <Treap> current_node, std::shared_ptr <Treap> & left,
            std::shared_ptr <Treap> & right);

 std::shared_ptr<Treap> merge(std::shared_ptr <Treap> left, std::shared_ptr <Treap> right);

 Treap(int key, int priority, std::shared_ptr <Treap> left = nullptr, std::shared_ptr <Treap> right = nullptr);

 void erase(int key, std::shared_ptr <Treap> & tree);

 int get_depth(std::shared_ptr <Treap> tree);

 int get_width(std::shared_ptr <Treap> tree);

 void get_width_of_level(std::shared_ptr<Treap> tree, int level, std::vector<int> &current_level);
};