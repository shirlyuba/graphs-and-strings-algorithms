#include "BellmanFord.h"


Bellman_Ford::Bellman_Ford(Graph new_G) { graph = new_G; };

bool Bellman_Ford::algorithm_Bellman_Ford(int start) {
  for (size_t i = 0; i < graph.get_count_of_vertices(); i++) {
    smallest_ways[i] = LONG_MAX;
  }
  smallest_ways[start] = 0;
  for (size_t k = 0; k < graph.get_count_of_vertices() - 1; k++) {
    for (size_t i = 0; i < graph.get_count_of_vertices(); i++) {
      for (size_t j = 0; j < graph.get_adjancy_list()[i].size(); j++) {
        if (smallest_ways[graph.get_adjancy_list()[i][j]] >
                smallest_ways[i] + graph.get_value(i, graph.get_adjancy_list()[i][j]))
          smallest_ways[graph.get_adjancy_list()[i][j]] = smallest_ways[i] +
                graph.get_value(i, graph.get_adjancy_list()[i][j]);
        }
    }
  }
  for (size_t i = 0; i < graph.get_count_of_vertices(); i++) {
    for (size_t j = 0; j < graph.get_adjancy_list()[i].size(); j++) {
      if (smallest_ways[graph.get_adjancy_list()[i][j]] > 
		      smallest_ways[i] + graph.get_value(i, graph.get_adjancy_list()[i][j]))
        return true;
    }
  }
  return false;
}

bool Bellman_Ford::find_negative_circle() {
  for (size_t i = 0; i < graph.get_count_of_vertices(); i++) {
    if (algorithm_Bellman_Ford(i)) {
      return true;
    }
  }
  return false;
}
