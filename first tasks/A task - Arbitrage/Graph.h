#pragma once


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>


struct Edge {
  int start;
  int finish;
  double value;
} typedef;

class Graph {
 private:
  
  std::vector <Edge> edges;
	
  size_t count_of_vertices;
	
  std::vector <std::vector <double>> adjancy_matrix;
	
  std::vector <std::vector <double>> adjancy_list;
	
  std::vector <std::vector <double> > read_parameters_from_stdin(size_t N);

 public:
 
  Graph();
	
  explicit Graph(size_t new_count_of_vetrices);
	
  ~Graph();
	
  void set_count_of_vertices(size_t N);
	
  void read_adjancy_matrix_from_stdin();
	
  std::vector <std::vector <double>> get_adjancy_matrix();
	
  std::vector <std::vector <double>> get_adjancy_list();
	
  size_t get_count_of_vertices();
	
  void read_adjancy_list_from_stdin();
	
  bool existance_of_edge(int, int);
	
  void read_adjancy_matrix_from_file();
	
  void create_edge(int, int, double value = 0);
	
  double get_value(int start, int finish);
	
  void print_adjancy_matrix();
	
  void print_adjancy_list();
};
