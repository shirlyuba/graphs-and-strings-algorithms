#pragma once


#include <map>

#include "Graph.h"


class Bellman_Ford {
 private:

  Graph graph;
  
  std::map <int, double> smallest_ways;

 public:
  
  Bellman_Ford(Graph new_G);

  bool algorithm_Bellman_Ford(int start);

  bool find_negative_circle();
};

