#include "Graph.h"
#include "BellmanFord.h"


int main() {
  size_t count_of_vertices;
  std::cin >> count_of_vertices;
  Graph G(count_of_vertices);
  G.read_adjancy_matrix_from_stdin();
  Bellman_Ford BF(G);
  if (BF.find_negative_circle())
    std::cout << "Yes";
  else
    std::cout << "No";
  return 0;
}