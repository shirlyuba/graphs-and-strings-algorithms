#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <map>

class WorkOnStrings {
 private:

 std::string treated_string;

 std::vector <int> prefix_function;

 std::vector <int> z_function;

 std::vector <int> suffix_array;

 std::vector <int> LCP_array;

 void radix_sort(std::vector <int> & frequency_of_symbols);

 void radix_sort(std::vector <int> & frequence_of_stymbols, std::vector <int> & current_classes,
                 int current_numbers_of_class, int current_shift);

 public:

 WorkOnStrings(std::string new_treated_string);

 WorkOnStrings() {};

 void get_prefix_function();

 void init_prefix_function(const std::vector <int> & prefix_function);

 std::vector <int> return_prefix_function();

 void init_z_function(const std::vector <int> & z_function);

 void get_z_function();

 std::vector <int> return_z_function();

 void get_z_from_prefix();

 void get_prefix_from_z();

 std::vector <int> get_string_from_prefix_function();

 std::vector <int> get_string_from_z_function();

 std::string convert_vector_to_string(const std::vector <int> & vector_of_string);

 std::vector<int> find_substring(std::string substring);

 std::map<std::string, long long> find_substring_of_pattern(const std::string & pattern);

 void build_suffix_array();

 void build_LCP_array();

 int find_number_of_different_substrings();

 void print_in_stdout(const std::vector<int> & any_vector);

};