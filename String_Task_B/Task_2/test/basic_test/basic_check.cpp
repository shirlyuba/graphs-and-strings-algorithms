#include <WorkOnStrings.h>
#include "gtest/gtest.h"


TEST(basic_test, test_1) {
	WorkOnStrings smart_string("abcabcd");
	smart_string.get_prefix_function();
	std::vector <int> answer = {0, 0, 0, 1, 2, 3, 0};
	EXPECT_EQ(smart_string.return_prefix_function(), answer);
}

TEST(basic_test, test_2) {
	WorkOnStrings smart_string("abracadabra");
	smart_string.get_prefix_function();
	std::vector <int> answer = {0, 0, 0, 1, 0, 1, 0, 1, 2, 3, 4};
	EXPECT_EQ(smart_string.return_prefix_function(), answer);
}

TEST(basic_test, test_3) {
	WorkOnStrings smart_string("abacaba");
	smart_string.get_z_function();
	std::vector <int> answer = {7, 0, 1, 0, 3, 0 , 1};
	EXPECT_EQ(smart_string.return_z_function(), answer);
}

TEST(basic_test, test_4) {
	WorkOnStrings smart_string("abracadabra");
	smart_string.get_z_function();
	std::vector <int> answer = {11, 0, 0, 1, 0, 1, 0, 4, 0, 0, 1};
	EXPECT_EQ(smart_string.return_z_function(), answer);
}

TEST(basic_test, test_5) {
	WorkOnStrings smart_string("abracadabra");
	smart_string.get_prefix_function();
	smart_string.get_z_from_prefix();
	std::vector <int> answer = {11, 0, 0, 1, 0, 1, 0, 4, 0, 0, 1};
	EXPECT_EQ(smart_string.return_z_function(), answer);
}

TEST(basic_test, test_6) {
	WorkOnStrings smart_string("abracadabra");
	smart_string.get_z_function();
	smart_string.get_prefix_from_z();
	std::vector <int> answer = {0, 0, 0, 1, 0, 1, 0, 1, 2, 3, 4};
	EXPECT_EQ(smart_string.return_prefix_function(), answer);
}

TEST(basic_test, test_7) {
	std::vector <int> prefix_function = {0, 1, 0, 1, 2, 2, 3};
	WorkOnStrings smart_string;
	smart_string.init_prefix_function(prefix_function);
	std::string tested_string = smart_string.convert_vector_to_string(smart_string.get_string_from_prefix_function());
	std::string real_string = "aabaaab";
	EXPECT_EQ(tested_string, real_string);
}

TEST(basic_test, test_8) {
	std::vector <int> prefix_function = {0, 0, 0, 1, 0, 1, 0, 1, 2, 3, 4};
	WorkOnStrings smart_string;
	smart_string.init_prefix_function(prefix_function);
	std::string tested_string = smart_string.convert_vector_to_string(smart_string.get_string_from_prefix_function());
	std::string real_string = "abbacacabba";
	EXPECT_EQ(tested_string, real_string);
}

TEST(basic_test, test_9) {
	std::vector <int> z_function = {11, 0, 0, 1, 0, 1, 0, 4, 0, 0, 1};
	WorkOnStrings smart_string;
	smart_string.init_z_function(z_function);
	std::string tested_string = smart_string.convert_vector_to_string(smart_string.get_string_from_z_function());
	std::string real_string = "abbacacabba";
	EXPECT_EQ(tested_string, real_string);
}

TEST(basic_test, test_10) {
	std::vector <int> z_function = {7, 0, 0, 1, 2, 0, 1};
	WorkOnStrings smart_string;
	smart_string.init_z_function(z_function);
	std::string tested_string = smart_string.convert_vector_to_string(smart_string.get_string_from_z_function());
	std::string real_string = "abbaaba";
	EXPECT_EQ(tested_string, real_string);
}