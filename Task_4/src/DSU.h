#pragma once

#include <vector>

struct DSU {

	DSU(int n);

	std::vector <int> leader;

	std::vector <int> rank;

	void union_sets(int first_vert, int second_vert);

	int find_set(int vert);

};
