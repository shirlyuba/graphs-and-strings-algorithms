#include "Kruskal.h"
#include "DSU.h"
#include <algorithm>


int find_MST(int count_of_vertices, const std::vector <Edge> & edges) {
	int answer = 0;
	std::sort(edges.begin(), edges.end(), [](const Edge & a, const Edge & b) {
        return a.value < b.value;
	});
	DSU current_DSU(count_of_vertices);
	for (size_t i = 0; i < edges.size(); ++i) {
        Edge current_edge = edges[i];
		int start_of_edge = current_edge.start;
		int finish_of_edge = current_edge.finish;
		if (current_DSU.find_set(start_of_edge) != current_DSU.find_set(finish_of_edge)) {
			answer += current_edge.value;
			current_DSU.union_sets(start_of_edge, finish_of_edge);
		}
	}
	return answer;
}

void print_values_of_MST_in_file(int result) {
	std::ofstream file("Kruskal2.txt");
	file << result;
	file.close();
}
