#pragma once

#include "Graph.h"

int find_MST(int, const std::vector & <Edge>);

void print_values_of_MST_in_file(int);
