#pragma once


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>


struct Edge {
	int start;
	int finish;
	double value;
};

class Graph {
 private:

  std::vector <Edge> edges;

  size_t count_of_vertices;

 public:
  Graph();

  explicit Graph(size_t new_count_of_vertices);

  std::vector <Edge> get_edges();

  size_t get_count_of_vertices();

  void create_edge(int start, int finish, int value = 0);

  void read_graph_from_file();
};