
#include "DSU.h"



DSU::DSU(int n) {
	for (int i = 0; i < n; ++i) {
		leader.push_back(i);
		rank.push_back(0);
	}
}

int DSU::find_set(int vert) {
	if (vert == leader[vert])
		return vert;
	return leader[vert] = find_set(leader[vert]);
}

void DSU::union_sets(int first_vert, int second_vert) {
	int first_set = find_set(first_vert);
	int second_set = find_set(second_vert);
	if (first_set != second_set) {
		if (rank[first_set] < rank[second_set])
			std::swap(first_set, second_set);
		leader[second_set] = first_set;
		if (rank[first_set] == rank[second_set])
			++rank[first_set];
	}
}
