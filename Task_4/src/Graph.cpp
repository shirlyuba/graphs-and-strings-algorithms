#include "Graph.h"

void Graph::read_graph_from_file() {
	std::ifstream file("Kruskal.txt");
	int count_of_edges;
	file >> count_of_vertices >> count_of_edges;
	for (int i = 0; i < count_of_edges; ++i) {
		int start, finish, value;
		file >> start >> finish >> value;
		create_edge(start - 1, finish - 1, value);
	}
	file.close();
};


Graph::Graph() {};

Graph::Graph(size_t new_count_of_vertices) {
	count_of_vertices = new_count_of_vertices;
}

void Graph::create_edge(int start, int finish, int value) {
	Edge new_edge;
	new_edge.start = start;
	new_edge.finish = finish;
	new_edge.value = value;
	edges.push_back(new_edge);
}

size_t Graph::get_count_of_vertices() {
	return count_of_vertices;
}

std::vector <Edge> Graph::get_edges() {
	return edges;
}
