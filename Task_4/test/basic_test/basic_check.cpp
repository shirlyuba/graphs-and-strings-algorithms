#include "gtest/gtest.h"
#include <Kruskal.h>


TEST(basic_test, simple_test) {
	Graph g(3);
	g.create_edge(0, 1, 1);
	g.create_edge(1, 2, 1);
	EXPECT_EQ(2, find_MST(g.get_count_of_vertices(), g.get_edges()));
}


TEST(basic_test, test_from_example) {
	Graph g(4);
	g.create_edge(0, 1, 1);
	g.create_edge(1, 2, 2);
	g.create_edge(2, 3, 5);
	g.create_edge(3, 0, 4);
	EXPECT_EQ(7, find_MST(g.get_count_of_vertices(), g.get_edges()));
}

TEST(basic_test, test_1) {
	Graph g(6);
	g.create_edge(0, 1, 1);
	g.create_edge(1, 2, 2);
	g.create_edge(0, 2, 4);
	g.create_edge(3, 2, 2);
	g.create_edge(3, 5, 3);
	g.create_edge(5, 4, 1);
	g.create_edge(3, 4, 2);
EXPECT_EQ(8, find_MST(g.get_count_of_vertices(), g.get_edges()));
}

TEST(basic_test, test_2) {
	Graph g(12);
	g.create_edge(0, 1, 7);
	g.create_edge(1, 2, 5);
	g.create_edge(2, 3, 3);
	g.create_edge(3, 8, 1);
	g.create_edge(3, 9, 2);
	g.create_edge(2, 4, 7);
	g.create_edge(9, 10, 7);
	g.create_edge(3, 11, 4);
	g.create_edge(5, 11, 3);
	g.create_edge(5, 7, 2);
  g.create_edge(5, 6, 1);
	EXPECT_EQ(42, find_MST(g.get_count_of_vertices(), g.get_edges()));
}


TEST(basic_test, test_3) {
	Graph g(5);
	g.create_edge(1, 2, 5);
	g.create_edge(1, 2, 2);
	g.create_edge(2, 3, 4);
	g.create_edge(2, 4, 3);
	g.create_edge(3, 4, 6);
	g.create_edge(0, 3, 20);
	g.create_edge(0, 4, 10);
EXPECT_EQ(19, find_MST(g.get_count_of_vertices(), g.get_edges()));
}


TEST(basic_test, hard_test) {
	Graph g;
	g.read_graph_from_file();
	EXPECT_EQ(58692, find_MST(g.get_count_of_vertices(), g.get_edges()));
}

