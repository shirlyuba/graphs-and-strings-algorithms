#include "src/Kruskal.h"

int main() {
	Graph current_graph;
	current_graph.read_graph_from_file();
	print_values_of_MST_in_file(
		find_MST(current_graph.get_count_of_vertices(),current_graph.get_edges()));
 return 0;
}