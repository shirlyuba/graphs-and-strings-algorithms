#include "Graph.h"


bool Graph::read_graph(int & source, int & target, int & count_of_lines) {
	std::ifstream file("input.txt");
	int count_of_edges;
	std::string lines_of_parametres;
	for (int i = 0; i < count_of_lines; ++i) {
		std::getline(file, lines_of_parametres);
	}
	file >> count_of_vertices;
	++count_of_lines;
	if (count_of_vertices == 0) {
		file.close();
		return false;
	}
	file >> source >> target >> count_of_edges;
	++count_of_lines;
	for (int i = 0; i < count_of_edges; i++) {
		int start, finish, value;
		file >> start >> finish >> value;
		++count_of_lines;
		create_edge(start - 1, finish - 1, value);
	}
	file.close();
	return true;
}

Graph::Graph() {};

Graph::Graph(size_t new_count_of_vertices) {
  count_of_vertices = new_count_of_vertices;
}

void Graph::create_edge(int start, int finish, int value) {
	if (capacity_of_edges.find({start, finish}) != capacity_of_edges.end()) {
		capacity_of_edges[{start, finish}] += value;
		capacity_of_edges[{finish, start}] += value;
		return;
	}
	capacity_of_edges[{start, finish}] = value;
	capacity_of_edges[{finish, start}] = value;
}

int Graph::get_capacity(int start, int finish) {
  return capacity_of_edges[{start, finish}];
}

std::vector <std::vector <int> > Graph::get_adjancy_list() {
  return adjancy_list;
}

void Graph::create_adjancy_list() {
	adjancy_list.resize(count_of_vertices);
	for (auto item : capacity_of_edges) {
		adjancy_list[item.first.first].push_back(item.first.second);
	}
}

void Graph::change_capacity(int start, int finish, double new_capacity) {
	capacity_of_edges[{start, finish}] -= new_capacity;
	capacity_of_edges[{finish, start}] += new_capacity;
}

size_t Graph::get_count_of_vertices() {
	return count_of_vertices;
}
