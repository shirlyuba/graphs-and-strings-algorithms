#include "EdmondsKarp.h"
#include <queue>
#include <algorithm>


std::vector<int> find_path(int start, int finish, Graph & current_graph) {
	std::queue<int> queue_of_vertex;
	queue_of_vertex.push(start);
	std::vector<int> all_paths (current_graph.get_count_of_vertices());
	std::vector<int> parents (current_graph.get_count_of_vertices());
	std::vector<bool> used (current_graph.get_count_of_vertices());
	used[start] = true;
	parents[start] = -1;
	while (!queue_of_vertex.empty()) {
		int current_vertex = queue_of_vertex.front();
		queue_of_vertex.pop();
		std::vector <int> adjancy_vertices = current_graph.get_adjancy_list()[current_vertex];
		for (int i = 0; i < adjancy_vertices.size(); i++) {
			if (!used[adjancy_vertices[i]]) {
				if (current_graph.get_capacity(current_vertex, adjancy_vertices[i]) > 0) {
					used[adjancy_vertices[i]] = true;
					queue_of_vertex.push(adjancy_vertices[i]);
					parents[adjancy_vertices[i]] = current_vertex;
					all_paths[adjancy_vertices[i]] = all_paths[current_vertex] + 1;
				}
			}
		}
	}
	if (!used[finish]) {
		return std::vector<int>();
	} else {
		std::vector <int> path;
		for (int current_vertex = finish; current_vertex != -1; current_vertex = parents[current_vertex]) {
			path.push_back(current_vertex);
		}
		std::reverse(path.begin(), path.end());
		return path;
	}
}

int push_flow(std::vector<int> & path, int start, int finish, Graph & current_graph) {
	int current_vertex = finish;
	int index_of_currnt_vertex = path.size() - 1;
	int min_capacity_in_path = current_graph.get_capacity(path[index_of_currnt_vertex - 1], current_vertex);
	while (index_of_currnt_vertex != 1) {
		--index_of_currnt_vertex;
		current_vertex = path[index_of_currnt_vertex];
		min_capacity_in_path = std::min(min_capacity_in_path, current_graph.get_capacity(path[index_of_currnt_vertex - 1], current_vertex));
	}
	current_vertex = finish;
	index_of_currnt_vertex = path.size() - 1;
	while (current_vertex != start) {
		int prev = path[index_of_currnt_vertex - 1];
		--index_of_currnt_vertex;
		current_graph.change_capacity(prev, current_vertex, min_capacity_in_path);
		current_vertex = prev;
	}
	return min_capacity_in_path;
}

int find_max_flow(int start, int finish, Graph & current_graph) {
	std::vector<int> path = find_path(start, finish, current_graph);
	int max_flow = 0;
	while (path.size() > 0) {
		max_flow += push_flow(path, start, finish, current_graph);
		path = find_path(start, finish, current_graph);
	}
	return max_flow;
}

