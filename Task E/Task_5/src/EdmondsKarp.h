#pragma once

#include "Graph.h"
#include <map>

int find_max_flow(int start, int finish, Graph & graph);

std::vector<int> find_path(int start, int finish, Graph & graph);

int push_flow(std::vector<int> & path, int start, int finish, Graph & graph);
