#pragma once


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>


class Graph {
 private:

  std::map<std::pair<int, int>, int> capacity_of_edges; // map <{start, finish}, capacity>
	
  size_t count_of_vertices;
	
  std::vector <std::vector <int>> adjancy_list;

 public:
  Graph();

  explicit Graph(size_t new_count_of_vertices);

  size_t get_count_of_vertices();

  std::vector <std::vector <int>> get_adjancy_list();

  void create_adjancy_list();

  bool read_graph(int & source, int & target, int & count_of_lines);
	
  void create_edge(int start, int finish, int capacity);

  int get_capacity(int start, int finish);

  void change_capacity(int start, int finish, double new_capacity);

};