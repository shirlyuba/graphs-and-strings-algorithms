#include <Graph.h>
#include <EdmondsKarp.h>

#include "gtest/gtest.h"


TEST(basic_test, test_1) {
	Graph G(4);
	G.create_edge(0, 1, 20);
	G.create_edge(0, 2, 10);
	G.create_edge(1, 2, 5);
	G.create_edge(1, 3, 10);
	G.create_edge(2, 3, 20);
	G.create_adjancy_list();
	EXPECT_EQ(25, find_max_flow(0, 3, G));
}

TEST(basic_test, test_2) {
	Graph G(6);
	G.create_edge(0, 1, 9);
	G.create_edge(0, 2, 9);
	G.create_edge(1, 2, 10);
	G.create_edge(1, 3, 8);
	G.create_edge(2, 3, 1);
	G.create_edge(2 ,4, 3);
	G.create_edge(3, 4, 8);
	G.create_edge(4, 5, 7);
	G.create_edge(3, 5, 10);
	G.create_adjancy_list();
	EXPECT_EQ(12, find_max_flow(0, 5, G));
}

TEST(basic_test, test_3) {
	Graph G(4);
	G.create_edge(0, 1, 20);
	G.create_edge(0, 2, 20);
	G.create_edge(1, 2, 1);
	G.create_edge(1, 3, 20);
	G.create_edge(2, 3, 20);
	G.create_adjancy_list();
	EXPECT_EQ(40, find_max_flow(0, 3, G));
}

TEST(basic_test, test_4) {
	Graph G(6);
	G.create_edge(0, 1, 16);
	G.create_edge(0, 2, 13);
	G.create_edge(2, 1, 4);
	G.create_edge(1, 3, 12);
	G.create_edge(2, 4, 14);
	G.create_edge(2, 3, 9);
	G.create_edge(4, 3, 7);
	G.create_edge(4, 5, 4);
	G.create_edge(3, 5, 20);
	G.create_adjancy_list();
	EXPECT_EQ(24, find_max_flow(0, 5, G));
}
