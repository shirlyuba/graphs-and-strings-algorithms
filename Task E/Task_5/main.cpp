#include "EdmondsKarp.h"

int main() {
	std::ofstream file("output.txt");
	if (!file) {
		std::cerr << "Can`t open file!" << std::endl;
		exit(1);
	}
	int count_of_lines_in_file = 0;
	int source, target;
	while (true) {
		Graph G;
		if (!G.read_graph(source, target, count_of_lines_in_file))
			break;
		G.create_adjancy_list();
		int answer = find_max_flow(source - 1, target - 1, G);
		file << answer << " ";
	}
	file.close();
  return 0;
}
