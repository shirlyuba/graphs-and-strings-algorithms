#include <iostream>
#include <algorithm>

#include "WorkOnStrings.h"

const int MAX_SIZE_OF_ALPHABET = 26;

void WorkOnStrings::get_prefix_function() {
	size_t count_of_letters = treated_string.length();
	prefix_function.assign(count_of_letters, 0);
	for (int i = 1; i < count_of_letters; ++i) {
		int j = prefix_function[i-1];
		while (j > 0 && treated_string[i] != treated_string[j]) {
			j = prefix_function[j-1];
		}
		if (treated_string[i] == treated_string[j]) {
			++j;
		}
		prefix_function[i] = j;
	}
}

std::vector<int> WorkOnStrings::get_string_from_prefix_function() {
	if (WorkOnStrings::prefix_function.empty()) {
		std::string string_from_stdin;
		std::getline(std::cin, string_from_stdin);
		std::istringstream iss(string_from_stdin);
		int element_of_prefix_function;
		while (iss >> element_of_prefix_function) {
			prefix_function.push_back(element_of_prefix_function);
		}
	}
	std::vector<int> found_string;
	found_string.resize(prefix_function.size());
	if (prefix_function.size() == 0) {
		return found_string;
	}
	for (int i = 1; i < prefix_function.size(); ++i) {
		for (int index_in_alphabet = 0; index_in_alphabet < MAX_SIZE_OF_ALPHABET; ++index_in_alphabet) {
			int last_letter = prefix_function[i - 1];
			while (found_string[last_letter] < index_in_alphabet && last_letter > 0) {
				last_letter = prefix_function[last_letter - 1];
			}
			if (found_string[last_letter] == index_in_alphabet) {
				last_letter++;
			}
			if (prefix_function[i] == last_letter) {
				found_string[i] = index_in_alphabet;
				break;
			}
		}
	}
	return found_string;
}

std::vector<int> WorkOnStrings::find_substring(std::string substring) {
	std::vector <int> indexes_of_substrings;
	treated_string = substring + '#' + treated_string;
	get_prefix_function();
	size_t count_of_letters_in_substring = substring.length();
	for (int i = 0; i < treated_string.length(); i++) {
		if (prefix_function[count_of_letters_in_substring + 1 + i] == count_of_letters_in_substring) {
			indexes_of_substrings.push_back(i - count_of_letters_in_substring + 1);
		}
	}
	return indexes_of_substrings;
}

void WorkOnStrings::get_z_function() {
	size_t count_of_letters = treated_string.length();
	z_function.assign(count_of_letters, 0);
	int right = 0;
	int left = 0;
	for (int i = 0; i < count_of_letters; ++i) {
		if (i <= right) {
			z_function[i] = std::min(right - i + 1, z_function[i - left]);
 		}
		while (i + z_function[i] < count_of_letters &&
		       treated_string[z_function[i]] == treated_string[i + z_function[i]]) {
			++z_function[i];
		}
		if (i + z_function[i] - 1 > right) {
			left = i;
			right = i + z_function[i] - 1;
		}
	}
}

void WorkOnStrings::get_z_from_prefix() {
	z_function.resize(prefix_function.size());
	for (int i = 1; i < prefix_function.size(); ++i) {
		if (prefix_function[i] > 0) {
			z_function[i - prefix_function[i] + 1] = prefix_function[i];
		}
	}
	z_function[0] = z_function.size();
	int i = 1;
	while (i < z_function.size()) {
		int t = i;
		if (z_function[i] > 0) {
			for (int j = 1; j < z_function[i]; ++j) {
				if (z_function[i + j] > z_function[j]) {
					break;
				}
				z_function[i + j] = std::min(z_function[j], z_function[i] - j);
				t = i + j;
			}
		}
		i = t + 1;
	}
}


std::vector<int> WorkOnStrings::get_string_from_z_function() {
	if (z_function.empty()) {
		std::string string_from_stdin;
		std::getline(std::cin, string_from_stdin);
		std::istringstream iss(string_from_stdin);
		int element_of_z_function;
		while (iss >> element_of_z_function) {
			z_function.push_back(element_of_z_function);
		}
	}
	get_prefix_from_z();
	std::vector<int> found_string;
	found_string.resize(prefix_function.size());
	if (prefix_function.size() == 0) {
		return found_string;
	}
	for (int i = 1; i < prefix_function.size(); ++i) {
		for (int index_in_alphabet = 0; index_in_alphabet < MAX_SIZE_OF_ALPHABET; ++index_in_alphabet) {
			int last_letter = prefix_function[i - 1];
			while (found_string[last_letter] != index_in_alphabet && last_letter > 0) {
				last_letter = prefix_function[last_letter - 1];
			}
			if (found_string[last_letter] == index_in_alphabet) {
				++last_letter;
			}
			if (prefix_function[i] == last_letter) {
				found_string[i] = index_in_alphabet;
				break;
			}
		}
	}
	return found_string;
}


void WorkOnStrings::get_prefix_from_z() {
	prefix_function.assign(z_function.size(), 0);
	for (int i = 1; i < z_function.size(); ++i) {
		for (int j = z_function[i] - 1; j >= 0; --j) {
			if (prefix_function[i + j] > 0) {
				break;
			} else {
				prefix_function[i + j] = j + 1;
			}
		}
	}
}

std::string WorkOnStrings::convert_vector_to_string(const std::vector<int> & vector_of_string) {
	std::string treated_string;
	treated_string.resize(vector_of_string.size());
	std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
	for (int i = 0; i < treated_string.length(); ++i) {
		treated_string[i] = alphabet[vector_of_string[i]];
	}
	return treated_string;
}

std::map <std::string, long long> WorkOnStrings::find_substring_of_pattern(const std::string & pattern) {
	std::map <std::string, long long> result;
	std::string current_substring;
	for (long long i = 0; i < pattern.length(); ++i) {
		if (pattern[i] != '?') {
			current_substring.push_back(pattern[i]);
		} else {
			if (current_substring.length()) {
				result[current_substring] = i - current_substring.length();
				current_substring.clear();
			}
		}
	}
	if (current_substring.length()) {
		result[current_substring] = pattern.length() - current_substring.length();
	}
	return result;
}

WorkOnStrings::WorkOnStrings(std::string new_treated_string) {
	treated_string = new_treated_string;
}

void WorkOnStrings::build_suffix_array() {
	treated_string += '#';
	std::vector <int> frequency_of_symbols (MAX_SIZE_OF_ALPHABET + 1);
	std::vector <int> numbers_of_classes (treated_string.length());
	std::vector <int> current_classes;
	suffix_array.assign(treated_string.length(), 0);
	radix_sort(frequency_of_symbols);
	unsigned int current_number_of_classes = 1;
	for (int i = 1; i < treated_string.length(); ++i) {
		if (treated_string[suffix_array[i]] != treated_string[suffix_array[i - 1]]) {
			++current_number_of_classes;
		}
		numbers_of_classes[suffix_array[i]] = current_number_of_classes - 1;
	}
	current_classes = numbers_of_classes;
	for (int current_shift = 0; (1 << current_shift) < treated_string.length(); ++current_shift) {
		radix_sort(frequency_of_symbols, current_classes, current_number_of_classes, current_shift);
		numbers_of_classes.assign(treated_string.length(), 0);
		numbers_of_classes[suffix_array[0]] = 0;
		current_number_of_classes = 1;
		for (int i = 1; i < treated_string.length(); ++i) {
			int left_of_current = suffix_array[i];
			int right_of_current = (suffix_array[i] + (1 << current_shift)) % treated_string.length();
			int left_of_prev = suffix_array[i - 1];
			int right_of_prev = (suffix_array[i - 1] + (1 << current_shift)) % treated_string.length();
			if (current_classes[left_of_current] != current_classes[left_of_prev] ||
                                            current_classes[right_of_current] != current_classes[right_of_prev]) {
				++current_number_of_classes;
			}
			numbers_of_classes[suffix_array[i]] = current_number_of_classes - 1;
		}
		current_classes = numbers_of_classes;
	}
}

void WorkOnStrings::build_LCP_array() {
	LCP_array.resize(treated_string.length());
	std::vector <int> positions_of_suff_array;
	positions_of_suff_array.resize(treated_string.length());
	for (int i = 0; i < treated_string.length(); ++i) {
		positions_of_suff_array[suffix_array[i]] = i;
	}
	int length_of_max_prefix = 0;
	for (int start_of_current_suffix = 0; start_of_current_suffix < treated_string.length(); ++start_of_current_suffix) {
		if (length_of_max_prefix > 0) {
			--length_of_max_prefix;
		}
		if (positions_of_suff_array[start_of_current_suffix] == treated_string.length() - 1) {
			LCP_array[treated_string.length() - 1] = -1;
			length_of_max_prefix = 0;
		} else {
			int start_of_compared_suffix = suffix_array[positions_of_suff_array[start_of_current_suffix] + 1];
			while (std::max(start_of_current_suffix + length_of_max_prefix, start_of_compared_suffix + length_of_max_prefix)
                < treated_string.length() && treated_string[start_of_current_suffix + length_of_max_prefix] ==
                                                   treated_string[start_of_compared_suffix + length_of_max_prefix]) {
				++length_of_max_prefix;
			}
			LCP_array[positions_of_suff_array[start_of_current_suffix]] = length_of_max_prefix;
		}
	}
	treated_string.erase(treated_string.end() - 1);
	suffix_array.erase(suffix_array.begin());
	LCP_array.erase(LCP_array.end() - 1);
}

int WorkOnStrings::find_number_of_different_substrings() {
	int number_of_different_substrings = 0;
	for (int i = 0; i < treated_string.length(); ++i) {
		number_of_different_substrings += treated_string.length() - suffix_array[i];
		number_of_different_substrings -= LCP_array[i];
	}
	return number_of_different_substrings;
}

void WorkOnStrings::radix_sort(std::vector<int> &frequency_of_symbols) {
	for (int i = 0; i < treated_string.length(); ++i) {
		if (treated_string[i] == '#') {
			++frequency_of_symbols[0];
		} else {
			++frequency_of_symbols[treated_string[i] - 'a' + 1];
		}
	}
	for (int i = 1; i < MAX_SIZE_OF_ALPHABET + 1; i++) {
		frequency_of_symbols[i] += frequency_of_symbols[i - 1];
	}
	for (int i = 0; i < treated_string.length(); ++i) {
		if (treated_string[i] == '#') {
			suffix_array[--frequency_of_symbols[0]] = i;
		} else {
			suffix_array[--frequency_of_symbols[treated_string[i] - 'a' + 1]] = i;
		}
	}
}

void WorkOnStrings::radix_sort(std::vector <int> & frequency_of_symbols, std::vector <int> & current_classes,
                               int current_number_of_classes, int current_shift) {
	frequency_of_symbols.assign(current_number_of_classes, 0);
	std::vector <int> suffix_array_for_current_shift;
	suffix_array_for_current_shift.assign(treated_string.length(), 0);
	for (int i = 0; i < treated_string.length(); ++i) {
		suffix_array_for_current_shift[i] = suffix_array[i] - (1 << current_shift);
		if (suffix_array_for_current_shift[i] < 0) {
			suffix_array_for_current_shift[i] += treated_string.length();
		}
	}
	for (int i = 0; i < treated_string.length(); ++i) {
		++frequency_of_symbols[current_classes[suffix_array_for_current_shift[i]]];
	}
	for (int i = 1; i < current_number_of_classes; ++i) {
		frequency_of_symbols[i] += frequency_of_symbols[i - 1];
	}
	for (int i = treated_string.length() - 1; i >= 0; --i) {
		suffix_array[--frequency_of_symbols[current_classes[suffix_array_for_current_shift[i]]]]
			= suffix_array_for_current_shift[i];
	}
}

void WorkOnStrings::print_in_stdout(const std::vector<int> & any_vector) {
	for (int item : any_vector) {
		std::cout << item << " ";
	}
}

std::vector<int> WorkOnStrings::return_prefix_function() {
	return prefix_function;
}

std::vector<int> WorkOnStrings::return_z_function() {
	return z_function;
}

void WorkOnStrings::init_prefix_function(const std::vector<int> & prefix_function) {
	this->prefix_function = prefix_function;
}

void WorkOnStrings::init_z_function(const std::vector<int> & z_function) {
	this->z_function = z_function;
}

std::vector<int> WorkOnStrings::return_suffix_array() {
	return suffix_array;
}

std::vector<int> WorkOnStrings::return_LCP_array() {
	return LCP_array;
}

