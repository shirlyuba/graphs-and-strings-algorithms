#include <iostream>
#include "WorkOnStrings.h"

int main() {
	std::string treated_string;
	std::cin >> treated_string;
	WorkOnStrings smart_string(treated_string);
	smart_string.build_suffix_array();
	smart_string.build_LCP_array();
	std::cout << smart_string.find_number_of_different_substrings();
	return 0;
}