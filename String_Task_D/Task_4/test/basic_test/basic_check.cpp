#include <WorkOnStrings.h>
#include "gtest/gtest.h"


TEST(basic_test, test_1) {
	WorkOnStrings smart_string("abracadabra");
	smart_string.build_suffix_array();
	smart_string.build_LCP_array();
	EXPECT_EQ(smart_string.find_number_of_different_substrings(), 54);
}

TEST(basic_test, test_2) {
	WorkOnStrings smart_string("abacabadabacaba");
	smart_string.build_suffix_array();
	smart_string.build_LCP_array();
	EXPECT_EQ(smart_string.find_number_of_different_substrings(), 85);
}

TEST(basic_test, test_3) {
	WorkOnStrings smart_string("abcde");
	smart_string.build_suffix_array();
	smart_string.build_LCP_array();
	EXPECT_EQ(smart_string.find_number_of_different_substrings(), 15);
}

TEST(basic_test, test_4) {
	WorkOnStrings smart_string("abracadabra");
	smart_string.build_suffix_array();
	std::vector <int> tested_suffix_array = smart_string.return_suffix_array();
	tested_suffix_array.erase(tested_suffix_array.begin());
	std::vector <int> real_suffix_array = {10, 7, 0, 3, 5, 8, 1, 4, 6, 9, 2};
	EXPECT_EQ(tested_suffix_array, real_suffix_array);
}

TEST(basic_test, test_5) {
	WorkOnStrings smart_string("abbbabbccab");
	smart_string.build_suffix_array();
	std::vector <int> tested_suffix_array = smart_string.return_suffix_array();
	tested_suffix_array.erase(tested_suffix_array.begin());
	std::vector <int> real_suffix_array = {9, 0, 4, 10, 3, 2, 1, 5, 6, 8, 7};
	EXPECT_EQ(tested_suffix_array, real_suffix_array);
}

TEST(basic_test, test_6) {
	WorkOnStrings smart_string("mississippi");
	smart_string.build_suffix_array();
	std::vector <int> tested_suffix_array = smart_string.return_suffix_array();
	tested_suffix_array.erase(tested_suffix_array.begin());
	std::vector <int> real_suffix_array = {10, 7, 4, 1, 0, 9, 8, 6, 3, 5, 2};
	EXPECT_EQ(tested_suffix_array, real_suffix_array);
}

TEST(basic_test, test_7) {
	WorkOnStrings smart_string("aabaaca");
	smart_string.build_suffix_array();
	smart_string.build_LCP_array();
	std::vector <int> tested_LCP_array = smart_string.return_LCP_array();
	std::vector <int> real_LCP_array = {0, 1, 2, 1, 1, 0, 0};
	EXPECT_EQ(tested_LCP_array, real_LCP_array);
}

TEST(basic_test, test_8) {
	WorkOnStrings smart_string("mississippi");
	smart_string.build_suffix_array();
	smart_string.build_LCP_array();
	std::vector <int> tested_LCP_array = smart_string.return_LCP_array();
	std::vector <int> real_LCP_array = {0, 1, 1, 4, 0, 0, 1, 0, 2, 1, 3};
	EXPECT_EQ(tested_LCP_array, real_LCP_array);
}
