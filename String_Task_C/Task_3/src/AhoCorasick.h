#pragma once

#include <vector>
#include <string>
#include <map>

using std::vector;

class Aho_Corasick {

 private:

 struct Node {

	 Node() {};

	 char symbol_to_parent;
	 bool is_finish = false;
	 long long parent = -1;
	 long long suffix_link = -1;
	 long long tight_link = -1;
	 long long index_of_substring = -1;
	 std::map<char, long long> child;
	 std::map<char, long long> conversion;

	 explicit Node(long long new_parent, char new_symbol) {
	  parent = new_parent;
	  symbol_to_parent = new_symbol;
	 }
 };

 vector<std::string> substrings;

 vector<Node> all_nodes;

 long long get_suffix_link(long long current_node);

 long long make_conversion(long long current_node, char symbol);

 long long get_tight_link(long long current_node);

 public:

 Aho_Corasick() {
  all_nodes.resize(1);
  all_nodes[0] = Node();
  all_nodes[0].suffix_link = 0;
 }

 void add_string(const std::string & substring);

 std::map <std::string, std::vector <long long>> find_substring_of_pattern(const std::string & pattern, int & count);

 vector<long long> find_positons_of_substrings(const std::string & treated_string, const std::string & pattern,
                                               std::map <std::string, vector <long long>> & substrings);

 void print_positions_of_pattern(const vector<long long> & indexes_of_pattern, int count_of_substrings);

 void update_indexes_of_pattern(vector<long long int> &indexes_of_pattern, long long int position_of_pattern,
                                long long int size_of_pattern);

 void find_possible_position(long long int number_of_symbol, long long int i, const std::string &pattern,
                             vector<long long int> &indexes_of_pattern,
                             std::map<std::string, vector<long long int>> &substrings_of_patterns);
};