#include <iostream>

#include "AhoCorasick.h"

long long Aho_Corasick::get_suffix_link(long long current_node) {
	if (all_nodes[current_node].suffix_link == -1) {
		if (!current_node || !all_nodes[current_node].parent) {
			all_nodes[current_node].suffix_link = 0;
		} else {
			all_nodes[current_node].suffix_link = make_conversion(get_suffix_link(all_nodes[current_node].parent),
			                                                      all_nodes[current_node].symbol_to_parent);
		}
	}
	return all_nodes[current_node].suffix_link;
}

long long Aho_Corasick::make_conversion(long long current_node, char symbol) {
	if (!all_nodes[current_node].conversion[symbol]) {
		if (all_nodes[current_node].child.find(symbol) != all_nodes[current_node].child.end()) {
			all_nodes[current_node].conversion[symbol] = all_nodes[current_node].child[symbol];
		} else {
			if (!current_node) {
				all_nodes[current_node].conversion[symbol] = 0;
			} else {
				all_nodes[current_node].conversion[symbol] = make_conversion(get_suffix_link(current_node), symbol);
			}
		}
	}
	return  all_nodes[current_node].conversion[symbol];
}

long long Aho_Corasick::get_tight_link(long long current_node) {
	if (all_nodes[current_node].tight_link == -1) {
		long long new_tight_link = get_suffix_link(current_node);
		if (!new_tight_link) {
			all_nodes[current_node].tight_link = 0;
		} else {
			if (all_nodes[new_tight_link].is_finish) {
				all_nodes[current_node].tight_link = new_tight_link;
			} else {
				all_nodes[current_node].tight_link = get_tight_link(new_tight_link);
			}
		}
	}
	return all_nodes[current_node].tight_link;
}

void Aho_Corasick::add_string(const std::string & substring) {
	long long current_node = 0;
	for (long long i = 0; i < substring.length(); ++i) {
		char current_symbol = substring[i];
		if (!all_nodes[current_node].child[current_symbol]) {
			all_nodes.push_back(Node(current_node, current_symbol));
			all_nodes[current_node].child[current_symbol] = all_nodes.size() - 1;
		}
		current_node = all_nodes[current_node].child[current_symbol];
	}
	all_nodes[current_node].is_finish = true;
	substrings.push_back(substring);
	all_nodes[current_node].index_of_substring = substrings.size() - 1;
}

void Aho_Corasick::find_possible_position(long long number_of_symbol, long long i, const std::string & pattern,
                                          std::vector <long long> & indexes_of_pattern,
                                          std::map <std::string, std::vector<long long>> & substrings_of_patterns) {
	for (long long j = number_of_symbol; j != 0; j = get_tight_link(j)) {
		if (all_nodes[j].is_finish) {
			for (auto pos_of_substring_in_pattern : substrings_of_patterns[substrings[all_nodes[j].index_of_substring]]) {
				long long pos_of_substring_in_treated_string = i + 1 - substrings[all_nodes[j].index_of_substring].length();
				long long pos_of_pattern = pos_of_substring_in_treated_string - pos_of_substring_in_pattern;
				update_indexes_of_pattern(indexes_of_pattern, pos_of_pattern, pattern.size());
			}
		}
	}
}

void Aho_Corasick::update_indexes_of_pattern(std::vector<long long> & indexes_of_pattern, long long position_of_pattern,
                                             long long size_of_pattern) {
	if (position_of_pattern >= 0 && position_of_pattern + size_of_pattern <= indexes_of_pattern.size()) {
		++indexes_of_pattern[position_of_pattern];
	}
}

std::vector <long long> Aho_Corasick::find_positons_of_substrings(const std::string & treated_string,
                                                                  const std::string & pattern,
                                              std::map <std::string, std::vector<long long>> & substrings_of_patterns) {
	long long number_of_symbol = 0;
	vector <long long> indexes_of_pattern;
	indexes_of_pattern.assign(treated_string.length(), 0);
	for (long long i = 0; i < treated_string.length(); ++i) {
		number_of_symbol = make_conversion(number_of_symbol, treated_string[i]);
		find_possible_position(number_of_symbol, i, pattern, indexes_of_pattern, substrings_of_patterns);
	}
	return indexes_of_pattern;
}

std::map<std::string, std::vector<long long>> Aho_Corasick::find_substring_of_pattern(const std::string & pattern,
                                                                                     int & count_of_substrings) {
	std::map <std::string, std::vector <long long>> result;
	std::string current_substring;
	for (long long i = 0; i < pattern.length(); ++i) {
		if (pattern[i] != '?') {
			current_substring.push_back(pattern[i]);
		} else {
			if (current_substring.length()) {
				result[current_substring].push_back(i - current_substring.length());
				++count_of_substrings;
				current_substring.clear();
			}
		}
	}
	if (current_substring.length()) {
		result[current_substring].push_back(pattern.length() - current_substring.length());
		++count_of_substrings;
	}
	return result;
}

void Aho_Corasick::print_positions_of_pattern(const vector<long long> &indexes_of_pattern,
                                              int count_of_substrings) {
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			std::cout << i << " ";
		}
	}
}