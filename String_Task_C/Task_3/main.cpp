#include <iostream>

#include "AhoCorasick.h"

int main() {
	std::string text, pattern;
	std::cin >> pattern >> text;
	if (text.length() < pattern.length()) {
		return 0;
	}
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern(pattern, count_of_substrings);
	if (substrings_of_pattern.empty()) {
		for (long long i = 0; i <= text.length() - pattern.length(); ++i) {
			std::cout << i << " ";
		}
		return 0;
	}
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	trie.print_positions_of_pattern(trie.find_positons_of_substrings(text, pattern, substrings_of_pattern),
	                                count_of_substrings);
	return 0;
}