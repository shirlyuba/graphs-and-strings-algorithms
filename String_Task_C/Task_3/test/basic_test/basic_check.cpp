#include <AhoCorasick.h>
#include "gtest/gtest.h"


TEST(basic_test, test_1) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("?a?a?", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {0};
	std::vector <long long> indexes_of_pattern = trie.find_positons_of_substrings("vadaad", "?a?a?", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}

TEST(basic_test, test_2) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("a?a", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {3, 5};
	std::vector <long long> indexes_of_pattern =
		trie.find_positons_of_substrings("abracadabra", "a?a", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}

TEST(basic_test, test_3) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("abc???abc", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {7, 10};
	std::vector <long long> indexes_of_pattern =
		trie.find_positons_of_substrings("aaabbbcabcabcabcabc", "abc???abc", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}

TEST(basic_test, test_4) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("hello?", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {0};
	std::vector <long long> indexes_of_pattern =
		trie.find_positons_of_substrings("helloworldhello", "hello?", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}

TEST(basic_test, test_5) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("?hello", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {9};
	std::vector <long long> indexes_of_pattern = trie.find_positons_of_substrings("helloworldhello", "?hello", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}

TEST(basic_test, test_6) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("a?", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {0, 1, 2, 3, 4};
	std::vector <long long> indexes_of_pattern = trie.find_positons_of_substrings("aaaaaa", "a?", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}

TEST(basic_test, test_7) {
	Aho_Corasick trie;
	int count_of_substrings = 0;
	std::map <std::string, std::vector <long long>> substrings_of_pattern =
		trie.find_substring_of_pattern("a??b?c?d?e?w??a", count_of_substrings);
	for (auto item : substrings_of_pattern) {
		trie.add_string(item.first);
	}
	std::vector <long long> answer = {2};
	std::vector <long long> indexes_of_pattern = trie.find_positons_of_substrings("braccbfcedteewqsa", "a??b?c?d?e?w??a", substrings_of_pattern);
	std::vector <long long> tested_answer;
	for (long long i = 0; i < indexes_of_pattern.size(); ++i) {
		if (indexes_of_pattern[i] == count_of_substrings) {
			tested_answer.push_back(i);
		}
	}
	EXPECT_EQ(tested_answer, answer);
}