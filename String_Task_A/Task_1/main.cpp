#include <iostream>
#include "WorkOnStrings.h"

int main() {
	std::string treated_string, substring;
	std::cin >> substring >> treated_string;
	WorkOnStrings smart_string(treated_string);
	smart_string.print_in_stdout(smart_string.find_substring(substring));
}
