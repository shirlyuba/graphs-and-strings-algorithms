#include <WorkOnStrings.h>
#include "gtest/gtest.h"


TEST(basic_test, test_1) {
	WorkOnStrings smart_string("aaa");
	std::vector <int> answer = {0, 1 ,2};
	EXPECT_EQ(smart_string.find_substring("a"), answer);
}

TEST(basic_test, test_2) {
	WorkOnStrings smart_string("ababbabbbcabdddd");
	std::vector <int> answer = {2 ,5};
	EXPECT_EQ(smart_string.find_substring("abb"), answer);
}

TEST(basic_test, test_3) {
	WorkOnStrings smart_string("heellohelloworldhelloholle");
	std::vector <int> answer = {6, 16};
	EXPECT_EQ(smart_string.find_substring("hello"), answer);
}

TEST(basic_test, test_4) {
	WorkOnStrings smart_string("englishisthemostimportantlanguageintheworld");
	std::vector <int> answer = {4, 7};
	EXPECT_EQ(smart_string.find_substring("is"), answer);
}

TEST(basic_test, test_5) {
	WorkOnStrings smart_string("primitivetest");
	std::vector <int> answer = {0};
	EXPECT_EQ(smart_string.find_substring("primitivetest"), answer);
}