#include"AStar.h"

int main() {
  State game;
  game.read_state_from_stdin();
  print_answer_in_stdin(algorithm_a_star_to_state(game), game);
  return 0;
}