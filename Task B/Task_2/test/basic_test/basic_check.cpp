#include "gtest/gtest.h"

#include <State.h>
#include <AStar.h>

TEST(basic_test, test_1) {
	std::vector <int> state = {1, 2, 3, 4, 5, 6, 7, 0, 8};
	State game(state);
	EXPECT_EQ(1, algorithm_a_star_to_state(game).get_count_of_transpositions());
}

TEST(basic_test, test_2) {
	std::vector <int> state = {0, 1, 6, 4, 3, 2, 7, 5, 8};
	State game(state);
	EXPECT_EQ(8, algorithm_a_star_to_state(game).get_count_of_transpositions());
}

TEST(basic_test, test_3) {
	std::vector <int> state = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	State game(state);
	EXPECT_EQ(22, algorithm_a_star_to_state(game).get_count_of_transpositions());
}

TEST(basic_test, test_4) {
	std::vector <int> state = {1, 2, 3, 8, 0, 4, 7, 6, 5};
	State game(state);
	EXPECT_EQ(false, algorithm_a_star_to_state(game).is_solvable());
}

TEST(basic_test, test_5) {
	std::vector <int> state = {0, 1, 2, 5, 6, 3, 4, 7 ,8};
	State game(state);
	EXPECT_EQ(8, algorithm_a_star_to_state(game).get_count_of_transpositions());
}

