#pragma once


#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <memory>


#define size_of_state 3
 

class State {
 private:

  State* parent;

  char symbol_from_parent;

  std::vector <int> current_state;

  int count_of_transpositions;
  
  int possible_count_of_transposition;
  
  int current_distance;
  
  unsigned int manhattan();

 public:

 State operator=(const State & state);

  State(const std::vector <int> & new_state);

  State(const State & state);

  State();

  ~State();

  std::vector <int> get_current_state();

  void set_distance(int new_distance);

  int get_distance();

  void set_possible_count_of_transpositions(int possible_transposition);

  int get_possible_count_of_transpositions();

  void set_count_of_transpositions(int new_count);

  int get_count_of_transpositions();

  unsigned int heuristic();
  
  void read_state_from_stdin();

  std::vector <State> find_next_state();

  bool is_solvable();
  
  bool is_solution();

  void set_symbol(char symbol);

  char get_symbol();

  State* get_parent();
};
