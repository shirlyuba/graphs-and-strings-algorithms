#include "Comparators.h"


bool Comparator_for_queue::operator()
  (State first_state, State second_state) {
    return first_state.get_distance() > second_state.get_distance();
}

bool Comparator_for_set::operator()
  (const std::vector <int> & first_vector, const std::vector <int> & second_vector) {
    for (size_t i = 0; i < first_vector.size(); i++) {
      if (first_vector[i] < second_vector[i])
        return true;
      if (first_vector[i] > second_vector[i])
        return false;
    }
    return false;
}