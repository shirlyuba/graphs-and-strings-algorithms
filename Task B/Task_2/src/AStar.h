#pragma once


#include"State.h"
#include"Comparators.h"


State algorithm_a_star_to_state(State & state);

void print_answer_in_stdin(State result_of_a_star, State start_state);