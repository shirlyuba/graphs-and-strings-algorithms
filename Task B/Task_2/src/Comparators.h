#pragma once


#include"State.h"


class Comparator_for_queue {
 public:
  bool operator() (State first_state, State second_state);
};

class Comparator_for_set {
 public:
  bool operator() (const std::vector <int> & first_vector, const std::vector <int> & second_vector);
};