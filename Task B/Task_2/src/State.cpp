#include"State.h"


unsigned int State::manhattan() {
  unsigned int result = 0;
  for (size_t i = 1; i < current_state.size() + 1; i++) {
    int x;
    if (current_state[i - 1] == 0) {
      x = size_of_state * size_of_state;
    } else {
      x = current_state[i - 1];
    }
    if (x != i) {
      int difference_in_rows = ((x - 1) % size_of_state) - ((i - 1) % size_of_state);
      int difference_in_cols = ((x - 1) / size_of_state) - ((i - 1) / size_of_state);
      result += abs(difference_in_rows) + abs(difference_in_cols);
    }
  }
  return result;
  }

std::vector <int> State::get_current_state() {
  return current_state;
}

void State::set_distance(int new_distance) {
  current_distance = new_distance;
}

int State::get_distance() {
  return current_distance;
}

void State::set_possible_count_of_transpositions(int possible_transposition) {
  possible_count_of_transposition = possible_transposition;
}

int State::get_possible_count_of_transpositions() {
  return possible_count_of_transposition;
}

void State::set_count_of_transpositions(int new_count) {
  count_of_transpositions = new_count;
}

int State::get_count_of_transpositions() {
  return count_of_transpositions;
}

void State::set_symbol(char symbol) {
  symbol_from_parent = symbol;
}

char State::get_symbol() {
  return symbol_from_parent;
}

State::State(const std::vector <int> & new_state) {
  parent = nullptr;
  current_state = new_state;
}

State::State() {
  parent = nullptr;
};

State::~State() {
  current_state.clear();
}

unsigned int State::heuristic() {
  return manhattan();
}

void State::read_state_from_stdin() {
  for (int i = 0; i < size_of_state * size_of_state; i++) {
    int x;
    std::cin >> x;
    current_state.push_back(x);
  }
}

std::vector <State> State::find_next_state() {
  std::vector <State> possible_state;
  int place_of_null = 0;
  for (size_t i = 0; i < current_state.size(); i++) {
    if (current_state[i] == 0) {
      place_of_null = i + 1;
      break;
    }
  }
  if ((place_of_null - 1) / size_of_state != 0) {
    State new_state = current_state;
    std::swap(new_state.current_state[place_of_null - 1], new_state.current_state[place_of_null - size_of_state - 1]);
    new_state.parent = this;
    new_state.set_symbol('U');
    possible_state.push_back(new_state);
  }
  if ((place_of_null - 1) / size_of_state != size_of_state - 1) {
    State new_state = current_state;
    std::swap(new_state.current_state[place_of_null - 1], new_state.current_state[place_of_null + size_of_state - 1]);
    new_state.parent = this;
    new_state.set_symbol('D');
    possible_state.push_back(new_state);
  }
  if ((place_of_null - 1) % size_of_state != 0) {
    State new_state = current_state;
    std::swap(new_state.current_state[place_of_null - 1], new_state.current_state[place_of_null - 2]);
    new_state.parent = this;
    new_state.set_symbol('L');
    possible_state.push_back(new_state);
  }
  if ((place_of_null - 1) % size_of_state != size_of_state - 1) {
    State new_state = current_state;
    std::swap(new_state.current_state[place_of_null - 1], new_state.current_state[place_of_null]);
    new_state.parent = this;
    new_state.set_symbol('R');
    possible_state.push_back(new_state);
  }
  return possible_state;
}

bool State::is_solvable() {
  int inversions_count = 0;
  for (size_t i = 0; i < current_state.size() - 1; i++) {
    for (size_t j = i + 1; j < current_state.size(); j++) {
	  if (current_state[i] > current_state[j] && current_state[i] != 0 && current_state[j] != 0)
	    inversions_count++;
      if (size_of_state % 2 == 0 && current_state[i] == 0)
        inversions_count += i / size_of_state + 1;
    }
  }
  return inversions_count % 2 == 0;
}

bool State::is_solution() {
  for (size_t i = 0; i < current_state.size() - 1; i++) {
    if (current_state[i] != i + 1)
      return false;
  }
  return true;
}

State::State(const State &state) {
  current_state = state.current_state;
  current_distance = state.current_distance;
  count_of_transpositions = state.count_of_transpositions;
  parent = state.parent;
  possible_count_of_transposition = state.possible_count_of_transposition;
  symbol_from_parent = state.symbol_from_parent;
}

State* State::get_parent() {
  return parent;
}

State State::operator=(const State &state) {
  parent = state.parent;
  current_state = state.current_state;
  current_distance = state.current_distance;
  count_of_transpositions = state.count_of_transpositions;
  possible_count_of_transposition = state.possible_count_of_transposition;
  symbol_from_parent = state.symbol_from_parent;
  return *this;
}
