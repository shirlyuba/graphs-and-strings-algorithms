#include <queue>
#include <algorithm>
#include <set>


#include"AStar.h"


State algorithm_a_star_to_state(State & state) {
  if (!state.is_solvable()) {
    return state;
  }
  std::priority_queue <State, std::vector<State>,
    Comparator_for_queue> unvisited_queue;
  std::set <std::vector<int>, Comparator_for_set> visited;
  state.set_count_of_transpositions(0);
  state.set_distance(state.get_count_of_transpositions() + state.heuristic());
  unvisited_queue.push(state);
  while (!unvisited_queue.empty()) {
    State current;
    current = unvisited_queue.top();
    if (current.is_solution()) {
      return current;
    }
    unvisited_queue.pop();
    visited.insert(current.get_current_state());
    auto next_states = current.find_next_state();
    for (size_t i = 0; i < next_states.size(); i++) {
      State current_child = next_states[i];
      current_child.set_possible_count_of_transpositions(current.get_count_of_transpositions() + 1);
      if (current_child.get_possible_count_of_transpositions() < current_child.get_count_of_transpositions()
        || visited.find(current_child.get_current_state()) == visited.end()) {
        current_child.set_count_of_transpositions(current_child.get_possible_count_of_transpositions());
        current_child.set_distance(current_child.get_count_of_transpositions() + current_child.heuristic());
        unvisited_queue.push(current_child);
      }
    }
  }
  return state;
}

void print_answer_in_stdin(State result_of_a_star, State start_state) {
  if (result_of_a_star.is_solution()) {
    std::cout << result_of_a_star.get_count_of_transpositions() << std::endl;
/*    std::vector <char> answer;
    State* current_parent = &result_of_a_star;
    while (start_state.get_current_state() != current_parent->get_current_state()) {
      answer.push_back(result_of_a_star.get_symbol());
      current_parent = result_of_a_star.get_parent();
    }
    std::reverse(answer.begin(), answer.end());
    for (auto item : answer) {
      std::cout << item;
    } */
  } else {
    std::cout << - 1;
  }
}
