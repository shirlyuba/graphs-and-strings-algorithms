#pragma once

#include "Graph.h"

int find_max_flow(Graph & gr);

bool find_path(Graph & gr, std::vector <int> & path, std::queue<int> & queue_of_vertex);

int push_flow(std::vector<int> & path, int current_vertex,
              Graph & current_graph, int min_capacity, std::vector<int> & parent);

void find_min_cat(Graph & current_graph, std::string & s, std::string & p);