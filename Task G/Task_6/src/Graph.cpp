#include "Graph.h"

Graph::Graph() {};

Graph::Graph(size_t new_count_of_vertices) {
  count_of_vertices = new_count_of_vertices;
}

int Graph::build_graph(const std::string & s, const std::string & p) {
	size_t length_of_p = p.length();
	size_t length_of_s = s.length();
	count_of_vertices = length_of_p + length_of_s + 2;
	int start_0 = count_of_vertices - 2;
	int finish_1 = count_of_vertices - 1;
	int ans = 0;
	for (int i = 0; i < length_of_s - length_of_p + 1; i++) {
		std::string sub_p = s.substr(i, i + length_of_p);
		for (int j = 0; j < length_of_p; j++) {
			if ((sub_p[j] == '0' && p[j] == '1') || (sub_p[j] == '1' && p[j] == '0')) {
				++ans;
				continue;
			}
			if (sub_p[j] == '?' && p[j] == '?') {
				create_edge(i+j, length_of_s + j, 1);
				continue;
			}
			if (sub_p[j] == '?' && p[j] != '?') {
				if (p[j] == '1') {
					create_edge(i+j, finish_1, 1);
				} else {
					create_edge(i+j, start_0 , 1);
				}
				continue;
			}
			if (sub_p[j] != '?' && p[j] == '?') {
				if (sub_p[j] == '1') {
					create_edge(length_of_s + j, finish_1, 1);
				} else {
					create_edge(length_of_s + j, start_0, 1);
				}
				continue;
			}
		}
	}
	return ans;
}

size_t Graph::get_count_of_vertices() {
	return count_of_vertices;
}

void Graph::create_edge(int start, int finish, int value) {
	if (edges.find({start, finish}) != edges.end()) {
		edges[{start, finish}].first += value;
		edges[{finish, start}].first += value;
		return;
	}
	edges[{start, finish}].first = value;
	edges[{finish, start}].first = value;
	edges[{start, finish}].second = 0;
	edges[{finish, start}].second = 0;
	adjancy_list[start].push_back(finish);
	adjancy_list[finish].push_back(start);
}

int Graph::get_capacity(int start, int finish) {
	return edges[{start, finish}].first;
}

std::unordered_map <int, std::vector <int> > Graph::get_adjancy_list() {
	return adjancy_list;
}

void Graph::change_flow(int start, int finish, double new_flow) {
	edges[{start, finish}].second += new_flow;
	edges[{finish, start}].second -= new_flow;
}

std::map<std::pair<int, int>, std::pair<int, int>> Graph::get_edges() {
	return edges;
}

int Graph::get_flow(int start, int finish) {
	return edges[{start, finish}].second;
}

bool Graph::rise_flow(int start, int finish) {
	return edges[{start, finish}].first > edges[{start, finish}].second;
}

