#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>


class Graph {
 private:

  std::map<std::pair <int, int>, std::pair<int, int> > edges;

  std::unordered_map<int, std::vector<int>> adjancy_list;
	
  size_t count_of_vertices;

 public:
  Graph();

  std::map<std::pair <int, int>, std::pair<int, int>> get_edges();

  int build_graph(const std::string & s, const std::string & p);

  size_t get_count_of_vertices();

  explicit Graph(size_t new_count_of_vertices);

  std::unordered_map <int, std::vector <int>> get_adjancy_list();

  void create_edge(int start, int finish, int capacity);

  int get_capacity(int start, int finish);

  void change_flow(int start, int finish, double new_capacity);

  int get_flow(int start, int finish);

  bool rise_flow(int start, int finish);
};