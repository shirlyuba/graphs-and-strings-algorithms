#include "Flow.h"

bool find_path(Graph & current_graph, std::vector<int> & path, std::queue<int> & queue_of_vertex) {
	queue_of_vertex.push(current_graph.get_count_of_vertices() - 2);
	path.assign(current_graph.get_count_of_vertices(), INT16_MAX);
	path[current_graph.get_count_of_vertices() - 2] = 0;
	while (!queue_of_vertex.empty()) {
		int current_vertex = queue_of_vertex.front();
		queue_of_vertex.pop();
		std::vector <int> adjancy_vertices = current_graph.get_adjancy_list()[current_vertex];
		for (int i = 0; i < adjancy_vertices.size(); i++) {
			if (current_graph.get_capacity(current_vertex, adjancy_vertices[i]) >
			    current_graph.get_flow(current_vertex, adjancy_vertices[i]) &&
			    path[adjancy_vertices[i]] == INT16_MAX) {
				queue_of_vertex.push(adjancy_vertices[i]);
				path[adjancy_vertices[i]] = path[current_vertex] + 1;
			}
		}
	}
	return path[current_graph.get_count_of_vertices() - 1] != INT16_MAX;
}

int push_flow(std::vector<int> & path, int current_vertex,
              Graph & current_graph, int min_capacity, std::vector<int> & parent) {
	if (current_vertex == current_graph.get_count_of_vertices() - 1 || min_capacity == 0) {
		return min_capacity;
	}
	for (auto & item = parent[current_vertex]; item < current_graph.get_count_of_vertices(); ++item) {
		if (path[item] == path[current_vertex] + 1) {
			int flow = push_flow(path, item, current_graph,
			                     std::min(min_capacity, current_graph.get_capacity(current_vertex, item)
			                                            - current_graph.get_flow(current_vertex, item)), parent);
			if (flow != 0) {
				current_graph.change_flow(current_vertex, item, flow);
				return flow;
			}
		}
	}
	return 0;
}

int find_max_flow(Graph & current_graph) {
	int max_flow = 0;
	int flow;
	std::vector <int> path;
	std::vector<int> parent;
	std::queue<int> queue_of_vertex;
	int start = current_graph.get_count_of_vertices() - 2;
	while (find_path(current_graph, path, queue_of_vertex)) {
		parent.assign(current_graph.get_count_of_vertices(), 0);
		flow = push_flow(path, start, current_graph, INT16_MAX, parent);
		while (flow != 0) {
			max_flow += flow;
			flow = push_flow(path, start, current_graph, INT16_MAX, parent);
		}
	}
	return max_flow;
}

void find_min_cat(Graph & current_graph, std::string & s, std::string & p) {
	std::queue<int> queue_of_vertex;
	std::unordered_map<int, bool> visited;
	int start_0 = current_graph.get_count_of_vertices() - 2;
	queue_of_vertex.push(start_0);
	visited[start_0] = true;
	while (!queue_of_vertex.empty()) {
		int current_vertex = queue_of_vertex.front();
		queue_of_vertex.pop();
		std::vector <int> adjancy_vertices = current_graph.get_adjancy_list()[current_vertex];
		for (int i = 0; i < adjancy_vertices.size(); i++) {
			if (visited[adjancy_vertices[i]] != true && current_graph.rise_flow(current_vertex, adjancy_vertices[i])) {
				visited[adjancy_vertices[i]] = true;
				queue_of_vertex.push(adjancy_vertices[i]);
			}
		}
	}
	std::vector<bool> used;
	used.assign(s.length() + p.length() + 2, false);
	used[used.size() - 2] = true;
	used[used.size() - 1] = true;
	for (auto item : current_graph.get_edges()) {
		if (used[item.first.first] == true) {
			continue;
		}
		used[item.first.first] = true;
		if (item.first.first < s.length()) {
			if (visited[item.first.first]) {
				s[item.first.first] = '0';
				continue;
			} else {
				s[item.first.first] = '1';
				continue;
			}
		} else {
			if (visited[item.first.first]) {
				p[item.first.first - s.length()] = '0';
			continue;
			} else {
				p[item.first.first - s.length()] = '1';
			}
		}
	}
}

