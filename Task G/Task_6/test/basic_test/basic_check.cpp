#include "gtest/gtest.h"
#include <Graph.h>
#include <Flow.h>


TEST(basic_test, test_1) {
int Hamming_distance = 0;
	std::string s = "000??";
	std::string p = "1?";
	Graph Hamming_Graph;
	Hamming_distance += Hamming_Graph.build_graph(s, p);
	Hamming_distance += find_max_flow(Hamming_Graph);
	find_min_cat(Hamming_Graph, s, p);
	EXPECT_EQ(4, Hamming_distance);
	EXPECT_EQ("00010", s);
	EXPECT_EQ("10", p);
}

TEST(basic_test, test_2) {
	int Hamming_distance = 0;
	std::string s = "101?0?1??";
	std::string p = "1?";
	Graph Hamming_Graph;
	Hamming_distance += Hamming_Graph.build_graph(s, p);
	Hamming_distance += find_max_flow(Hamming_Graph);
	find_min_cat(Hamming_Graph, s, p);
	EXPECT_EQ(4, Hamming_distance);
	EXPECT_EQ("101101111", s);
	EXPECT_EQ("11", p);
}

TEST(basic_test, test_3) {
	int Hamming_distance = 0;
	std::string s = "00???111000111111111111111??????????1??00???????????1???0????11???001??";
	std::string p = "1?0??1?";
	Graph Hamming_Graph;
	Hamming_distance += Hamming_Graph.build_graph(s, p);
	Hamming_distance += find_max_flow(Hamming_Graph);
	find_min_cat(Hamming_Graph, s, p);
	EXPECT_EQ(105, Hamming_distance);
	EXPECT_EQ("00111111000111111111111111111111111111100111111111111111011111111100111", s);
	EXPECT_EQ("1101111", p);
}

TEST(basic_test, test_4) {
	int Hamming_distance = 0;
	std::string s = "???????????????????????????";
	std::string p = "???";
	Graph Hamming_Graph;
	Hamming_distance += Hamming_Graph.build_graph(s, p);
	Hamming_distance += find_max_flow(Hamming_Graph);
	find_min_cat(Hamming_Graph, s, p);
	EXPECT_EQ(0, Hamming_distance);
	EXPECT_EQ("111111111111111111111111111", s);
	EXPECT_EQ("111", p);
}
