#include "Flow.h"

int main() {
	int Hamming_distance = 0;
	std::string s, p;
	std::cin >> s >> p;
	Graph Hamming_Graph;
	Hamming_distance += Hamming_Graph.build_graph(s, p);
	Hamming_distance += find_max_flow(Hamming_Graph);
	find_min_cat(Hamming_Graph, s, p);
	std::cout << Hamming_distance << std::endl << s << std::endl << p;
  return 0;
}