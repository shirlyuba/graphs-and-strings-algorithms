#include "gtest/gtest.h"


#include "SegmentTree.h"


TEST(basic_test, test_1) {
	std::vector <int> segment = {1, 2, 3, 4, 5};
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	EXPECT_EQ(segment_tree.find_second_statistics(1, 5), 2);
}

TEST(basic_test, test_2) {
	std::vector <int> segment = {9, 100, 1, 2, -2, 100, 500, 2, 3, 4,};
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	EXPECT_EQ(segment_tree.find_second_statistics(6, 10), 3);
}

TEST(basic_test, test_3) {
	std::vector <int> segment = {-1, 1, -1, 1, -1, 2};
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	EXPECT_EQ(segment_tree.find_second_statistics(4, 6), 1);
}

TEST(basic_test, test_4) {
	std::vector <int> segment = {0, -3, 4, 5, 6, 100, -2, 4, 5, 10, 33, 1};
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	EXPECT_EQ(segment_tree.find_second_statistics(1, 12), -2);
}

TEST(basic_test, test_5) {
	std::vector <int> segment = {11, 1, 2, 34, -3, 22, 33, 2};
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	EXPECT_EQ(segment_tree.find_second_statistics(3, 8), 2);
}

TEST(basic_test, test_6) {
	std::vector <int> segment = {5, 4, 3, 18, 1, 9, 8, 6, 7, 5};
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	EXPECT_EQ(segment_tree.find_second_statistics(2, 9), 3);
}