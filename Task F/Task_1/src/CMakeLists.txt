set(CMAKE_CXX_STANDARD 14)

set(SOURCE_LIB SegmentTree.cpp InputOutputTreatment.cpp)

add_library(Task1_Lib STATIC ${SOURCE_LIB})