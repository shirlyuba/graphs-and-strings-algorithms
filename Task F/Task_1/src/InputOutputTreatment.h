#pragma once


#include "SegmentTree.h"


void read_segment_from_stdin(std::vector<int> & segment, int size_of_segment);

void print_second_statistics_in_stdout(SegmentTree & segment_tree, int count_of_diaposons);