#pragma once


#include <vector>


class SegmentTree {

 private:

 struct Node {
	 int min;
	 int second_min;
 };

 std::vector <Node> tree;

 void find_in_leaves(int &left_min, int &left_second_min, int &right_min, int &right_second_min, int &left, int &right);

 void find_in_tree(int &left_min, int &left_second_min, int &right_min, int &right_second_min, int &left, int &right);

 public:

 void build_tree(std::vector <int> & segment);

 int find_second_statistics(int left, int right);
};