#include <cmath>
#include <algorithm>


#include "SegmentTree.h"


void SegmentTree::build_tree(std::vector<int> & segment) {
	size_t size = (size_t)(1 << (int)((log(segment.size() - 1) / log(2)) + 1));
	segment.resize(size, std::numeric_limits<int>::max());
	tree.resize(2 * size - 1, {std::numeric_limits<int>::max(), std::numeric_limits<int>::max()});
	for (int i = 0; i < segment.size(); ++i) {
		tree[segment.size() - 1 + i] = { segment[i], std::numeric_limits<int>::max()};
	}
	for (int i = segment.size() - 2; i >= 0; --i) {
		int min = std::min(tree[2 * i + 2].min, tree[2 * i + 1].min);
		int possible_second_min;
		if (tree[2 * i + 1].min == min) {
			possible_second_min = tree[2 * i + 2].min;
		} else {
			possible_second_min = tree[2 * i + 1].min;
		}
		int second_min = std::min(tree[2 * i + 1].second_min, std::min(tree[2 * i + 2].second_min, possible_second_min));
		tree[i] = {min, second_min};
	}
}

int SegmentTree::find_second_statistics(int left, int right) {
	int new_index = (tree.size() - 1) / 2;
	left += new_index - 1;
	right += new_index - 1;
	int left_min = std::numeric_limits<int>::max();
	int right_min = std::numeric_limits<int>::max();
	int left_second_min = std::numeric_limits<int>::max();
	int right_second_min = std::numeric_limits<int>::max();
	if (right - left == 1) {
		return std::max(tree[left].min, tree[right].min);
	}
	find_in_leaves(left_min, left_second_min, right_min, right_second_min, left, right);
	while (right - left > 1) {
		find_in_tree(left_min, left_second_min, right_min, right_second_min, left, right);
	}
	if (left_min == right_min) {
		return left_min;
	} else {
		if (left_min < right_min) {
			return std::min(left_second_min, right_min);
		} else {
			return std::min(right_second_min, left_min);
		}
	}
}

void SegmentTree::find_in_leaves(int &left_min, int &left_second_min, int &right_min, int &right_second_min,
                                 int & left, int & right) {
	if (right & 1) {
		right_min = tree[right].min;
	} else {
		right_min = std::min(tree[right].min, tree[right - 1].min);
		right_second_min = std::max(tree[right].min, tree[right - 1].min);
	}
	if (left & 1) {
		left_min = std::min(tree[left].min, tree[left + 1].min);
		left_second_min = std::max(tree[left].min, tree[left + 1].min);
	} else {
		left_min = tree[left].min;
	}
	left = (left - 1) / 2;
	right = (right - 1) / 2;
}

void SegmentTree::find_in_tree(int &left_min, int &left_second_min, int &right_min, int &right_second_min,
                               int &left, int &right) {
	if (!(right & 1)) {
		if (right_min < tree[right - 1].min) {
			if (right_second_min > tree[right - 1].min) {
				right_second_min = tree[right - 1].min;
			}
		} else {
			right_second_min = std::min(right_min, tree[right - 1].second_min);
			right_min = tree[right - 1].min;
		}
	}
	if (left & 1) {
		if (left_min < tree[left + 1].min) {
			if (left_second_min > tree[left + 1].min) {
				left_second_min = tree[left + 1].min;
			}
		} else {
			left_second_min = std::min(left_min, tree[left + 1].second_min);
			left_min = tree[left + 1].min;
		}
	}
	left = (left - 1) / 2;
	right = (right - 1) / 2;
};