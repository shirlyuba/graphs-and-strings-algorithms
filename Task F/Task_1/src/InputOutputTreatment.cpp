#include <iostream>


#include "InputOutputTreatment.h"


void read_segment_from_stdin(std::vector<int> & segment, int size_of_segment) {
	for (int i = 0; i < size_of_segment; ++i) {
		int element;
		std::cin >> element;
		segment.push_back(element);
	}
}

void print_second_statistics_in_stdout(SegmentTree & segment_tree, int count_of_diaposons) {
	for (int i = 0; i < count_of_diaposons; ++i) {
		int left, right;
		std::cin >> left >> right;
		std::cout << segment_tree.find_second_statistics(left, right) << std::endl;
	}
}