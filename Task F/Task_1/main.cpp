#include <iostream>


#include "SegmentTree.h"
#include "InputOutputTreatment.h"


int main() {
	int size_of_segment, count_of_diaposons;
	std::cin >> size_of_segment >> count_of_diaposons;
	std::vector <int> segment;
	read_segment_from_stdin(segment, size_of_segment);
	SegmentTree segment_tree;
	segment_tree.build_tree(segment);
	print_second_statistics_in_stdout(segment_tree, count_of_diaposons);
	return 0;
}